import { createRequire } from "module";
const require = createRequire(import.meta.url); // для прцювання старих методів підключення у ES замість імопрту

const gulp = require("gulp");
const concat = require("gulp-concat");
const sass = require("gulp-sass")(require("sass"));
const uglify = require("gulp-uglify");
const minify = require("gulp-clean-css");
const autoPref = require("gulp-autoprefixer");
const browserSync = require("browser-sync").create();
const ttf2woff2 = require("gulp-ttf2woff2");
import imagemin from "gulp-imagemin";
import { deleteSync } from "del";

function styles() {
  return gulp
    .src("./src/styles/style.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(autoPref({ cascade: false }))
    .pipe(minify({ compatibility: "ie8" }))
    .pipe(concat("all.min.css"))
    .pipe(gulp.dest("./dist/styles/"))
    .pipe(browserSync.stream());
}
function scripts() {
  return gulp
    .src("./src/scripts/blocks/*.js")
    .pipe(concat("all.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./dist/scripts/"))
    .pipe(browserSync.stream());
}
function minImg() {
  return gulp
    .src("./src/images/*")
    .pipe(imagemin())
    .pipe(gulp.dest("./dist/images"));
}
function fonts() {
  return gulp
    .src(["./src/fonts/*.ttf"])
    .pipe(ttf2woff2())
    .pipe(gulp.dest("./dist/fonts/"));
}

function watch() {
  browserSync.init({
    server: {
      baseDir: "./",
    },
  });

  gulp.watch("./src/styles/*.scss", styles);
  gulp.watch("./src/styles/blocks/*.scss", styles);
  gulp.watch("./src/scripts/blocks/*.js", scripts);
  gulp.watch("./*.html", gulp.series(browserSync.reload, minImg));
}
function cleanDist(done) {
  deleteSync(["dist/**"]);
  done();
}

gulp.task("dev", watch);
gulp.task("build", gulp.series(cleanDist, styles, scripts, fonts, minImg));
