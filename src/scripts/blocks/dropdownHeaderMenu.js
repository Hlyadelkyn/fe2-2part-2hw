"use strict";

let headerNavBtn = document.querySelector(".mobile-header__btn");
let headerNav = document.querySelector(".header-navbar");
let btnDecActv = document.querySelector(".btn__decoration--active");
let btnDecPass = document.querySelector(".btn__decoration--passive");

function showNav() {
  headerNav.classList.toggle("active");
  btnDecActv.classList.toggle("active");
  btnDecPass.classList.toggle("active");
}
